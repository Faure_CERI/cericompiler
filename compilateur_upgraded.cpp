//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;


char current, lookedAhead;                // Current char    
int NLookedAhead = 0;


void ReadChar(void){
    if(NLookedAhead>0){
        current=lookedAhead;    // Char has already been read
        NLookedAhead--;
    }
    else
        // Read character and skip spaces until 
        // non space character is read
        while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
    NLookedAhead++;
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := {Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


void Letter(void){
	if((current<'0')||(current>'9'))
		Error("Letter expected");		   // Letter expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}

void ComparativExpression(void){
	if(current=='='||current=='?'||current=='<'||current=='>')
		ReadChar();
	else
		Error("Comparativ Operator expected");		
}

void Comparison(void){
	char opcomp;
	SimpleExpression();
	if (current == '?' || current == '<' || current == '>'){
		opcomp = current;
		ComparativExpression();
		SimpleExpression();
		cout << "pop % rbx" << "\t # résultat de l'expression 2" << endl;
		cout << "pop % rax" << "\t # résultat de l'expression 1" << endl;
		cout << "\t cmpq %rax, %rbx" << endl;
		switch(opcomp){
			case '?': 
					  cout << "\t je faux" << endl;
					  cout << "\t jne vrai" << endl;
					  break;
			case '<':
					  
					  cout << "\t jg faux" << endl;
					  cout << "\t jng vrai" << endl;
					  break;
					  
			case '>':
					  cout << "\t jl faux" << endl;
					  cout << "\t jnl vrai" << endl;
					  break;
		}
	}
}




void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Additive operator expected");	   // Additive operator expected
}


void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Digit expected");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}


void Number(void){
	unsigned long long number = 0;
	Digit();
	while (current >= '0' && current <= '9'){
		number *= 10;
		number += Digit();
	}
	cout << "\t push $" << number << endl;
}

void MultiplicativeOperator(void){
	
}

void Expression(void);			// Called by Factor()

void Factor(void){
	if (current >= '0' && current <= '9')
		Number();
	else if (current.isalpha())
		Letter();
	else if (current == '(')
	{
		Expression();
	}
	else if (current == '!'){
		
	}
}


void SimpleExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		SimpleExpression();
		if(current!=')')
			Error("')' expected");		// ")" expected
		else
			ReadChar();
	}
	else
		Error("'(' or digit or letter expected");
}


void SimpleExpression(void){
	char adop;
	Term();
	while(current == '+' || current == '-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}
	while(current == '*' || current == '/' || current == '%' || current == '&&'){
		mulop = current;
		MultiplicativeOperator();
	}

}



void Expression(void){
	SimpleExpression();

}


void AssignmentStatement(void){
	if(current.isalpha()){
		Letter();
		char let = current;
		ReadChar();
		while(current == '=')
		{
			Expression();
		}
	}
	else{
		Error("alphabetic character expected");
	}
}

void Statement(void){
	AssignmentStatement();
}


void DeclarationPart(void){
	if (current != '[')
		Error("Character '[' expected");				//'[' Expected
	current = ReadChar();
	Letter();
	while(current == ','){
		current = ReadChar();
		Letter();
}

void StatementPart(void){
	Statement();
	
	
}

void Program(void){
	cout << "\t# Program's beginning" << endl;
	if (current == '['){ DeclarationPart(); }
	StatementPart();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	
	cout << "\t.data\t# reserving space for global variables" << endl;
	
	cout << "true:\t\t\t" << endl;
	cout << "push $1" << endl;
	cout << "jmp end" << endl;
	cout << "false:\t\t\t" << endl;
	cout << "push $0" << endl;
	cout << "jmp end" << endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	SimpleExpression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "end:" << endl;
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}

